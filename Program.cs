﻿using System;
using TheAlgorithm;

namespace lunch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Summer!");
            while (true)
            {
                Console.WriteLine("Number 1:");
                int number1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Number 2:");
                int number2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Sum is:");
                Console.WriteLine(ThaSummer.Sum(number1, number2));
            }
        }
    }
}
