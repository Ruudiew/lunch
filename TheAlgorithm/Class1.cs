﻿using System;

namespace TheAlgorithm
{
    public static class ThaSummer
    {
        public static int Sum(int number1, int number2)
        {
            return number1 + number2;
        }
    }
}
